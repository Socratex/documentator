package main.java.com.socratex.plugins.documentator.components.data;

public interface NoteContents {

    String getName();

    String getDescription();

    NoteType getNoteType();

}
