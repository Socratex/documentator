package main.java.com.socratex.plugins.documentator.components.data;

import main.java.com.socratex.plugins.documentator.components.data.file.NoteDetailsFile;
import main.java.com.socratex.plugins.documentator.framework.base.Serializable;
import main.java.com.socratex.plugins.documentator.framework.helpers.markdown.table.MDColumnAlign;
import main.java.com.socratex.plugins.documentator.framework.helpers.markdown.table.MDTableColumn;
import main.java.com.socratex.plugins.documentator.framework.ui.table.JColumnAlign;
import main.java.com.socratex.plugins.documentator.framework.ui.table.JTableColumn;

import java.util.ArrayList;
import java.util.Collection;

public class NoteEntry implements NoteInterface, Serializable<NoteEntry> {

    @JTableColumn(name = "Id", hidden = true)
    private Integer id;

    @JTableColumn(name = "Name")
    @MDTableColumn(name = "Name")
    private String name;

    @JTableColumn(name = "Description")
    @MDTableColumn(name = "Description")
    private String description;

    @JTableColumn(name = "Synonyms", align = JColumnAlign.CENTER)
    @MDTableColumn(name = "Synonyms", align = MDColumnAlign.CENTER)
    private Collection<Integer> synonyms = new ArrayList<>();

    @JTableColumn(name = "Links", align = JColumnAlign.CENTER)
    @MDTableColumn(name = "Links", align = MDColumnAlign.CENTER)
    private Collection<NoteContents> links = new ArrayList<>();

    public NoteEntry(Integer id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public NoteEntry(Integer id, String name, String description, Collection<Integer> synonyms, Collection<NoteDetailsFile> files) {
        this.id = id;
        this.name = name;
        this.synonyms = synonyms;
        this.description = description;
        this.links = links;
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public Collection<Integer> getSynonyms() {
        return synonyms;
    }

    @Override
    public Collection<NoteContents> getLinks() {
        return links;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public NoteType getNoteType() {
        return NoteType.ENTRY;
    }

    @Override
    public NoteEntry deserialize(String source) {
        //TODO socratex - FINISH ME - 2/13/19 5:53 PM
        return null;
    }

    @Override
    public String serialize(NoteEntry source) {
        //TODO socratex - FINISH ME - 2/13/19 5:53 PM
        return null;
    }

}
