package main.java.com.socratex.plugins.documentator.components.data;

import java.util.Collection;

public interface NoteInterface extends NoteContents {

    Integer getId();

    Collection<Integer> getSynonyms();

    Collection<NoteContents> getLinks();

}
