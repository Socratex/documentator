package main.java.com.socratex.plugins.documentator.components.data;

public enum NoteType {
    ENTRY, FILE, MAIN_FILE
}
