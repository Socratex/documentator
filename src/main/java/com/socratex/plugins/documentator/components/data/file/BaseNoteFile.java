package main.java.com.socratex.plugins.documentator.components.data.file;

import main.java.com.socratex.plugins.documentator.components.data.NoteContents;

public abstract class BaseNoteFile implements NoteContents {

    protected String name;

    protected String description;

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getDescription() {
        return this.description;
    }

}
