package main.java.com.socratex.plugins.documentator.components.data.file;

import main.java.com.socratex.plugins.documentator.components.data.NoteEntry;
import main.java.com.socratex.plugins.documentator.components.data.NoteType;
import main.java.com.socratex.plugins.documentator.framework.helpers.annotation.AnnotationReader;
import main.java.com.socratex.plugins.documentator.framework.helpers.markdown.MDParser;
import main.java.com.socratex.plugins.documentator.framework.helpers.markdown.table.MDTableColumnHelper;

import java.util.ArrayList;
import java.util.Collection;

public class MainNoteFile extends BaseNoteFile {

    public static final String MAIN_NOTE_FILE = "PROJECT.md";

    private static MainNoteFile instance = null;

    private Collection<String> beforeDoc = new ArrayList<>();

    private Collection<String> afterDoc = new ArrayList<>();

    private boolean loaded = false;

    private Collection<NoteEntry> notes = new ArrayList<>();

    private MainNoteFile(String name) {
        this.name = name;
    }

    public static MainNoteFile mainInstance() {
        if (instance == null) {
            instance = new MainNoteFile(MAIN_NOTE_FILE);
        }
        return instance;
    }

    public void addNote(NoteEntry note) {
        notes.add(note);
    }

    @Override
    public NoteType getNoteType() {
        return NoteType.MAIN_FILE;
    }

    public void persist(Void data) {
        //TODO socratex - FINISH ME - 2/21/19 5:48 PM
        MDParser parser = MDParser.instance();
        AnnotationReader reader = AnnotationReader.instance();
        String tableHeader = parser.createTable(MDTableColumnHelper.instance().getAnnotations(NoteEntry.class));
    }

    public Collection<NoteEntry> getNotes() {
        return notes == null ? new ArrayList<>() : notes;
    }

    public Collection<String> getBeforeDoc() {
        return beforeDoc;
    }

    public void setBeforeDoc(Collection<String> beforeDoc) {
        this.beforeDoc = beforeDoc;
    }

    public Collection<String> getAfterDoc() {
        return afterDoc;
    }

    public void setAfterDoc(Collection<String> afterDoc) {
        this.afterDoc = afterDoc;
    }

    public boolean isLoaded() {
        return loaded;
    }

    public void setLoaded(boolean loaded) {
        this.loaded = loaded;
    }

}
