package main.java.com.socratex.plugins.documentator.components.data.file;

import main.java.com.socratex.plugins.documentator.components.data.NoteType;

public class NoteDetailsFile extends BaseNoteFile {

    @Override
    public NoteType getNoteType() {
        return NoteType.FILE;
    }

}
