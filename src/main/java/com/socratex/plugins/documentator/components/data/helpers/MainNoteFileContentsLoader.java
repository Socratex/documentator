package main.java.com.socratex.plugins.documentator.components.data.helpers;

import com.intellij.notification.NotificationType;
import main.java.com.socratex.plugins.documentator.components.data.NoteEntry;
import main.java.com.socratex.plugins.documentator.components.data.file.MainNoteFile;
import main.java.com.socratex.plugins.documentator.framework.base.Loader;
import main.java.com.socratex.plugins.documentator.framework.base.Phase;
import main.java.com.socratex.plugins.documentator.framework.base.wrappers.CollectionExtractor;
import main.java.com.socratex.plugins.documentator.framework.helpers.files.TextFileManager;
import main.java.com.socratex.plugins.documentator.framework.helpers.jetbrains.NotificationHelper;
import main.java.com.socratex.plugins.documentator.framework.helpers.markdown.MarkdownUtils;
import main.java.com.socratex.plugins.documentator.framework.helpers.markdown.table.MDTableColumn;
import main.java.com.socratex.plugins.documentator.framework.helpers.markdown.table.MDTableColumnHelper;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class MainNoteFileContentsLoader implements Loader<MainNoteFile, Collection<String>> {

    public static final String DOC_START = "<!--Documentator-->";

    public static final String DOC_END = "<!--/Documentator-->";

    private static MainNoteFileContentsLoader instance = new MainNoteFileContentsLoader();

    private MainNoteFileContentsLoader() {
    }

    public static MainNoteFileContentsLoader instance() {
        return instance;
    }

    @Override
    public MainNoteFile load(Collection<String> lines) {
        MainNoteFile mainNote = MainNoteFile.mainInstance();
        List<String> before = new ArrayList<>();
        List<String> after = new ArrayList<>();

        Phase docPhase = Phase.BEFORE;
        Phase tablePhase = Phase.BEFORE;
        boolean hasValidHeader = false;
        int id = 0;

        for (String line : lines) {
            switch (docPhase) {
                case BEFORE:
                    if (line.equals(DOC_START)) {
                        docPhase = Phase.DURING;
                    } else {
                        before.add(line);
                    }
                    break;
                case DURING:
                    if (line.equals(DOC_END)) {
                        docPhase = Phase.AFTER;
                    } else {
                        switch (tablePhase) {
                            case BEFORE:
                                validateTableHeader(line);
                                hasValidHeader = true;
                                tablePhase = Phase.DURING;
                                break;
                            case DURING:
                                assert MarkdownUtils.isTableContentsDivider(line);
                                tablePhase = Phase.AFTER;
                                break;
                            case AFTER:
                                id++;
                                NoteEntry noteEntry = loadLine(line, id);
                                MainNoteFile.mainInstance().addNote(noteEntry);
                                break;
                        }
                    }
                    break;
                case AFTER:
                    after.add(line);
                    break;
            }
        }

        if (!hasValidHeader) {
            throwInvalidHeaderException(getColumnNames(getMDAnnotations()), new ArrayList<>());
        }

        mainNote.setBeforeDoc(before);
        mainNote.setAfterDoc(after);
        mainNote.setLoaded(true);
        return mainNote;
    }

    public void prepareDocumentationFile(String filePath) {
        TextFileManager textFileManager = TextFileManager.instance();
        File file = null;
        try {
            file = textFileManager.assureFileExists(filePath);
            if (0 == file.length()) {
                String fileContents = prepareEmptyContents();
                textFileManager.writeToFile(filePath, fileContents);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @NotNull
    private NoteEntry loadLine(String line, int id) {
        //TODO socratex - FINISH ME - 4/17/19 2:49 PM - encapsulate
        Collection<String> rowValues = MarkdownUtils.extractTableRowValues(line);
        return new NoteEntry(id, "Test name", "Test description");
    }

    @NotNull
    private String prepareEmptyContents() {
        String start = "<!--Documentator-->\n";
        String end = "<!--/Documentator-->\n***\n" +
                "<b>Note</b>:<br> You can alter this file, but please do NOT:\n" +
                "- change table structure (two first rows)\n" +
                "- remove documentator comment tag.";
        return start + prepareEmptyMarkdownTable() + end;
    }

    private void validateTableHeader(String line) {
        Collection<? super String> expectedHeaders = getColumnNames(getMDAnnotations());
        Collection<String> headerValues = MarkdownUtils.extractTableRowValues(line);
        if (!expectedHeaders.equals(headerValues)) {
            throwInvalidHeaderException(expectedHeaders, headerValues);
        }
    }

    private void throwInvalidHeaderException(Collection<? super String> expectedHeaders, Collection<String> headerValues) {
        String errorMessage = "Don't change your project documentation file manually.\n" +
                "MD table headers should be: " + expectedHeaders.toString() + "\n" +
                "Headers in your MD documentation file was: " + headerValues.toString();
        NotificationHelper.displayNotification(errorMessage, NotificationType.ERROR);
        throw new RuntimeException(errorMessage);
    }

    @NotNull
    private String prepareEmptyMarkdownTable() {
        Collection<MDTableColumn> annotations = getMDAnnotations();
        Collection<String> columnNames = getColumnNames(annotations);
        CollectionExtractor<MDTableColumn, String> dividerExtractor = CollectionExtractor.createFor(source -> source.align().getCellContent());
        Collection<String> columnDividers = dividerExtractor.extract(annotations);
        return "";
    }

    private Collection<String> getColumnNames(Collection<MDTableColumn> annotations) {
        return CollectionExtractor.<MDTableColumn, String>createFor(source -> source.name()).extract(annotations);
    }

    private Collection<MDTableColumn> getMDAnnotations() {
        return MDTableColumnHelper.instance().getAnnotations(NoteEntry.class);
    }

}
