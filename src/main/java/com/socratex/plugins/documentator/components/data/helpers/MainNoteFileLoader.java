package main.java.com.socratex.plugins.documentator.components.data.helpers;

import main.java.com.socratex.plugins.documentator.components.data.file.MainNoteFile;
import main.java.com.socratex.plugins.documentator.framework.base.Loader;
import main.java.com.socratex.plugins.documentator.framework.helpers.files.TextFileManager;

import java.util.Collection;
import java.util.List;

public class MainNoteFileLoader implements Loader<MainNoteFile, String> {

    private Loader<MainNoteFile, Collection<String>> contentsLoader;

    private static MainNoteFileLoader instance = new MainNoteFileLoader();

    private MainNoteFileLoader(Loader<MainNoteFile, Collection<String>> contentsLoader) {
        this.contentsLoader = contentsLoader;
    }

    private MainNoteFileLoader() {
        contentsLoader = MainNoteFileContentsLoader.instance();
    }

    public static MainNoteFileLoader defaultInstance() {
        return instance;
    }

    @Override
    public MainNoteFile load(String projectDocumentationFilePath) {
        MainNoteFileContentsLoader contentsLoader = MainNoteFileContentsLoader.instance();
        contentsLoader.prepareDocumentationFile(projectDocumentationFilePath);

        List<String> fileLines = TextFileManager.instance().readFromFile(projectDocumentationFilePath);
        return contentsLoader.load(fileLines);
    }

}
