package main.java.com.socratex.plugins.documentator.components.data.helpers;

import main.java.com.socratex.plugins.documentator.components.data.NoteContents;
import main.java.com.socratex.plugins.documentator.components.data.NoteType;
import main.java.com.socratex.plugins.documentator.framework.base.Extractor;

class NoteFilePathExtractor<From extends NoteContents> implements Extractor<From, String> {

    @Override
    public String extract(From source) {
        String path = source.getNoteType() == NoteType.MAIN_FILE ? "" : "notes/";
        String fileName = source.getName();
        return path + fileName;
    }

}
