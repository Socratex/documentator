package main.java.com.socratex.plugins.documentator.components.data.helpers;

import main.java.com.socratex.plugins.documentator.components.data.file.BaseNoteFile;
import org.jetbrains.annotations.NotNull;

import java.io.*;

public class NoteManager {

    private static NoteManager instance = new NoteManager();

    private NoteManager() {
    }

    public static NoteManager instance() {
        return instance;
    }

    public void saveFile(BaseNoteFile noteFile) {
        String filePath = new NoteFilePathExtractor<>().extract(noteFile);
        File file = new File(resolveBasePath() + filePath);
        OutputStream out = createOutputStream(file);
        NoteManager manager = NoteManager.instance();
        //TODO socratex - FINISH ME - 2/12/19 4:58 PM
    }

    private String resolveBasePath() {
        return "";
    }

    @NotNull
    private OutputStream createOutputStream(File file) {
        OutputStream out = null;
        try {
            out = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return out;
    }

}
