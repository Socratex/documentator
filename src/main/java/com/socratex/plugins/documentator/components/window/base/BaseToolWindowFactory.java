package main.java.com.socratex.plugins.documentator.components.window.base;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.openapi.wm.ToolWindowFactory;
import com.intellij.ui.content.Content;
import com.intellij.ui.content.ContentFactory;
import main.java.com.socratex.plugins.documentator.framework.ui.base.ContentProvider;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;

public abstract class BaseToolWindowFactory<C extends ContentProvider<? extends JComponent>> implements ToolWindowFactory {

    @Override
    public void createToolWindowContent(@NotNull Project project, @NotNull ToolWindow toolWindow) {
        initializeWindowContent(project, toolWindow);
        C contentCreator = createContentCreator(toolWindow);
        ContentFactory contentFactory = ContentFactory.SERVICE.getInstance();
        Content content = contentFactory.createContent(contentCreator.getContent(), "", false);
        toolWindow.getContentManager().addContent(content);
    }

    protected void initializeWindowContent(Project project, ToolWindow toolWindow) {
    }

    protected abstract C createContentCreator(ToolWindow toolWindow);

}
