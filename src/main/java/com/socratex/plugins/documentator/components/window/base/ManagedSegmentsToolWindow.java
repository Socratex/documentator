package main.java.com.socratex.plugins.documentator.components.window.base;

import main.java.com.socratex.plugins.documentator.framework.ui.base.ContentProvider;
import main.java.com.socratex.plugins.documentator.framework.ui.base.ManagedSegment;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

public abstract class ManagedSegmentsToolWindow<T extends JComponent> implements ContentProvider<T> {

    private List<ManagedSegment> managedSegments = new ArrayList<>();

    @Override
    public JComponent getContent() {
        initializeViewCompontents();
        if (managedSegments.isEmpty()) {
            addWindowSegments(managedSegments);
        }
        manageSegments();
        return getWindowContent();
    }

    protected void initializeViewCompontents() {
    }

    protected abstract JComponent getWindowContent();

    protected abstract void addWindowSegments(List<ManagedSegment> segments);

    private void manageSegments() {
        managedSegments.forEach(managedSegment -> managedSegment.manageSegment());
    }

}
