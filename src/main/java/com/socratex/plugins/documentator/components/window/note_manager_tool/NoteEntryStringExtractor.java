package main.java.com.socratex.plugins.documentator.components.window.note_manager_tool;

import main.java.com.socratex.plugins.documentator.components.data.NoteEntry;
import main.java.com.socratex.plugins.documentator.framework.base.Extractor;
import main.java.com.socratex.plugins.documentator.framework.ui.table.JTableColumnHelper;

import java.lang.reflect.Field;
import java.util.Collection;

public class NoteEntryStringExtractor implements Extractor<NoteEntry, String[]> {

    private static NoteEntryStringExtractor instance = new NoteEntryStringExtractor();

    private NoteEntryStringExtractor() {
    }

    public static NoteEntryStringExtractor instance() {
        return instance;
    }

    @Override
    public String[] extract(NoteEntry noteEntry) {
        return JTableColumnHelper.instance().getFieldsAsStrings(noteEntry, new FieldStringExtractor(noteEntry));
    }

    private static class FieldStringExtractor implements Extractor<Field, String> {

        private final Object object;

        public FieldStringExtractor(Object object) {
            this.object = object;
        }

        @Override
        public String extract(Field source) {
            try {
                source.setAccessible(true);
                Object valueObject = source.get(object);
                if (valueObject instanceof Collection) {
                    int size = ((Collection<?>) valueObject).size();
                    return size > 0 ? String.valueOf(size) : "";
                }
                if (valueObject.getClass().isArray()) {
                    int length = ((Object[]) valueObject).length;
                    return length > 0 ? String.valueOf(length) : "";
                }
                return String.valueOf(valueObject);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
                return "Error: " + e.getMessage();
            }
        }

    }

}
