package main.java.com.socratex.plugins.documentator.components.window.note_manager_tool;

import main.java.com.socratex.plugins.documentator.components.data.NoteEntry;
import main.java.com.socratex.plugins.documentator.components.data.file.MainNoteFile;
import main.java.com.socratex.plugins.documentator.components.window.base.ManagedSegmentsToolWindow;
import main.java.com.socratex.plugins.documentator.framework.base.Extractor;
import main.java.com.socratex.plugins.documentator.framework.base.Provider;
import main.java.com.socratex.plugins.documentator.framework.ui.base.ContentManager;
import main.java.com.socratex.plugins.documentator.framework.ui.base.DefaultWindowSegment;
import main.java.com.socratex.plugins.documentator.framework.ui.base.ManagedSegment;
import main.java.com.socratex.plugins.documentator.framework.ui.table.AnnotatedEntityTableManager;

import javax.swing.*;
import java.util.Collection;
import java.util.List;

public class NoteManagerToolWindow extends ManagedSegmentsToolWindow<JComponent> {

    private JPanel content;

    private JScrollPane scrollPane;

    private JTable dictionaryTable;

    @Override
    protected JComponent getWindowContent() {
        return content;
    }

    @Override
    protected void addWindowSegments(List<ManagedSegment> segments) {
        Provider<Collection<? extends NoteEntry>> notesProvider = () -> MainNoteFile.mainInstance().getNotes();
        Extractor<NoteEntry, String[]> noteContentsExtractor = NoteEntryStringExtractor.instance();

        ContentManager<JTable> manager = new AnnotatedEntityTableManager<>(NoteEntry.class, notesProvider, noteContentsExtractor);
        segments.add(new DefaultWindowSegment<>(dictionaryTable, manager));
    }

}
