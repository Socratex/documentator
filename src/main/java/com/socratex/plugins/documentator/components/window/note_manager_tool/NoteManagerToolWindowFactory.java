package main.java.com.socratex.plugins.documentator.components.window.note_manager_tool;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.wm.ToolWindow;
import main.java.com.socratex.plugins.documentator.components.data.file.MainNoteFile;
import main.java.com.socratex.plugins.documentator.components.data.helpers.MainNoteFileLoader;
import main.java.com.socratex.plugins.documentator.components.window.base.BaseToolWindowFactory;
import org.jetbrains.annotations.NotNull;

/**
 * https://github.com/JetBrains/intellij-sdk-docs/tree/master/code_samples/tool_window/src/myToolWindow
 */
public class NoteManagerToolWindowFactory extends BaseToolWindowFactory<NoteManagerToolWindow> {

    public static final String DEFAULT_DOCUMENTATION_DIRECTORY = "/documentation/";

    public static final String DEFAULT_FILE_LOCATION = DEFAULT_DOCUMENTATION_DIRECTORY + MainNoteFile.MAIN_NOTE_FILE;

    @Override
    protected NoteManagerToolWindow createContentCreator(ToolWindow toolWindow) {
        return new NoteManagerToolWindow();
    }

    @Override
    protected void initializeWindowContent(Project project, ToolWindow toolWindow) {
        if (!MainNoteFile.mainInstance().isLoaded()) {
            String projectDocumentationFilePath = getFilePath(project);
            MainNoteFileLoader.defaultInstance().load(projectDocumentationFilePath);
        }
    }

    @NotNull
    private String getFilePath(Project project) {
        //TODO socratex - FINISH ME - retrieve implementations path from plugin settings, encapsulate
        return project.getBasePath() + DEFAULT_FILE_LOCATION;
    }

}
