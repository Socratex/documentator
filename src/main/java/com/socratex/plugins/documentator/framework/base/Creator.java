package main.java.com.socratex.plugins.documentator.framework.base;

public interface Creator<From, To> {

    To create(From source);

}
