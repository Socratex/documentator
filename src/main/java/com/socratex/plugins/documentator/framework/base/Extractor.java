package main.java.com.socratex.plugins.documentator.framework.base;

public interface Extractor<From, To> {

    To extract(From source);

}
