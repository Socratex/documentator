package main.java.com.socratex.plugins.documentator.framework.base;

public interface Loader<Result, Params> {

    Result load(Params params);

}
