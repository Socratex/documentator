package main.java.com.socratex.plugins.documentator.framework.base;

public interface Persister<T> {

    void persist(T data);

}
