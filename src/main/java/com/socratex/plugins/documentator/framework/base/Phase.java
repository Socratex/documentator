package main.java.com.socratex.plugins.documentator.framework.base;

public enum Phase {
    BEFORE, DURING, AFTER
}
