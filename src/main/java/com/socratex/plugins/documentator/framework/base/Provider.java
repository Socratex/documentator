package main.java.com.socratex.plugins.documentator.framework.base;

public interface Provider<T> {

    T provide();

}
