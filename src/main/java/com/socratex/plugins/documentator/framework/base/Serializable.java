package main.java.com.socratex.plugins.documentator.framework.base;

public interface Serializable<T> {

    T deserialize(String source);

    String serialize(T source);

}
