package main.java.com.socratex.plugins.documentator.framework.base.wrappers;

import main.java.com.socratex.plugins.documentator.framework.base.Creator;
import main.java.com.socratex.plugins.documentator.framework.base.Extractor;

import java.util.ArrayList;
import java.util.Collection;

public class CollectionExtractor<From, To> implements Extractor<Collection<? extends From>, Collection<? extends To>> {

    private Extractor<? super From, ? extends To> entryExtractor;

    private Creator<Void, ? extends Collection<To>> collectionCreator;

    private CollectionExtractor(Extractor<? super From, ? extends To> entryExtractor, Creator<Void, Collection<To>> collectionCreator) {
        this.entryExtractor = entryExtractor;
        this.collectionCreator = collectionCreator;
    }

    public static <From, To> CollectionExtractor<From, To> createFor(Extractor<? super From, ? extends To> entryExtractor, Creator<Void, Collection<To>> collectionCreator) {
        return new CollectionExtractor<>(entryExtractor, collectionCreator);
    }

    public static <From, To> CollectionExtractor<From, To> createFor(Extractor<? super From, ? extends To> entryExtractor) {
        return new CollectionExtractor<>(entryExtractor, source -> new ArrayList<>());
    }

    @Override
    public Collection<To> extract(Collection<? extends From> source) {
        Collection<To> result = collectionCreator.create(null);
        source.forEach(from -> result.add(entryExtractor.extract(from)));
        return result;
    }

}
