package main.java.com.socratex.plugins.documentator.framework.base.wrappers;

import main.java.com.socratex.plugins.documentator.framework.base.Extractor;

public class ExtractorWrapper<From, Middle, To> implements Extractor<From, To> {

    private Extractor<? super From, ? extends Middle> sourceExtractor;

    private Extractor<? super Middle, ? extends To> resultExtractor;

    private ExtractorWrapper(Extractor<? super From, ? extends Middle> sourceExtractor, Extractor<? super Middle, ? extends To> resultExtractor) {
        this.sourceExtractor = sourceExtractor;
        this.resultExtractor = resultExtractor;
    }

    public static <From, Middle, To> ExtractorWrapper create(Extractor<? super From, ? extends Middle> sourceExtractor, Extractor<? super Middle, ? extends To> resultExtractor) {
        return new ExtractorWrapper(sourceExtractor, resultExtractor);
    }

    @Override
    public To extract(From source) {
        return resultExtractor.extract(sourceExtractor.extract(source));
    }

}
