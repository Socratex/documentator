package main.java.com.socratex.plugins.documentator.framework.base.wrappers;

import main.java.com.socratex.plugins.documentator.framework.base.Loader;
import main.java.com.socratex.plugins.documentator.framework.base.Provider;

public class LoadedValueProvider<R, P> implements Provider<R> {

    private Loader<R, P> loader;

    private P value;

    private LoadedValueProvider(Loader<R, P> loader, P value) {
        this.loader = loader;
        this.value = value;
    }

    public static <R, P> LoadedValueProvider<R, P> from(Loader<R, P> loader, P value) {
        return new LoadedValueProvider(loader, value);
    }

    public static <R> LoadedValueProvider<R, Void> from(Loader<R, Void> loader) {
        return new LoadedValueProvider(loader, null);
    }

    @Override
    public R provide() {
        return loader.load(value);
    }

}
