package main.java.com.socratex.plugins.documentator.framework.helpers;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

public class DataConverter {

    private static DataConverter instance = new DataConverter();

    private DataConverter() {
    }

    public static DataConverter instance() {
        return instance;
    }

    @NotNull
    public static String[] arrayCast(Object[] annotationsData, Class<String[]> clazz) {
        return Arrays.copyOf(annotationsData, annotationsData.length, clazz);
    }

    public <T> Object[] setToArray(Set<T> set) {
        List<T> array = new ArrayList<>();
        array.addAll(set);
        return array.toArray();
    }

}
