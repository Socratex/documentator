package main.java.com.socratex.plugins.documentator.framework.helpers;

import main.java.com.socratex.plugins.documentator.framework.base.Extractor;

public class StringTrimmer implements Extractor<String, String> {

    private static StringTrimmer instance = new StringTrimmer();

    public static StringTrimmer instance() {
        return instance;
    }

    @Override
    public String extract(String source) {
        return source.trim();
    }

}
