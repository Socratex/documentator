package main.java.com.socratex.plugins.documentator.framework.helpers.annotation;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.LinkedHashMap;
import java.util.Map;

public class AnnotationReader {

    private static AnnotationReader instance = new AnnotationReader();

    private AnnotationReader() {
    }

    public static AnnotationReader instance() {
        return instance;
    }

    public Map<String, Field> getAnnotatedFields(Class<?> clazz, Class<? extends Annotation> annotationClass) {
        Map<String, Field> annotationMap = new LinkedHashMap<>();
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(annotationClass)) {
                annotationMap.put(field.getName(), field);
            }
        }
        return annotationMap;
    }

}
