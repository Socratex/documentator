package main.java.com.socratex.plugins.documentator.framework.helpers.annotation;

import main.java.com.socratex.plugins.documentator.framework.base.Extractor;
import main.java.com.socratex.plugins.documentator.framework.helpers.DataConverter;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

public abstract class BaseAnnotatedFieldHelper<T extends Annotation> {

    private Map<String, Field> fields = null;

    private ArrayList<T> annotations;

    public String[] getFieldsAsStrings(Object entity, Extractor<? super Field, ? extends String> fieldValueExtractor) {
        Collection<Field> fields = getAnnotatedFields(entity.getClass()).values();
        Collection<String> values = new ArrayList<>();
        for (Field value : fields) {
            values.add(fieldValueExtractor.extract(value));
        }
        return DataConverter.arrayCast(values.toArray(), String[].class);
    }

    public Collection<T> getAnnotations(Class<?> entityClass) {
        if (annotations == null) {
            Class<T> annotationClass = getAnnotationClass();
            Collection<Field> values = getAnnotatedFields(entityClass).values();
            annotations = new ArrayList<>();
            values.forEach(field -> {
                T annotation = field.getAnnotation(annotationClass);
                annotations.add(annotation);
            });
        }
        return annotations;
    }

    protected abstract Class<T> getAnnotationClass();

    private Map<String, Field> getAnnotatedFields(Class<?> entityClass) {
        if (fields == null) {
            Class<? extends Annotation> annotationClass = getAnnotationClass();
            fields = AnnotationReader.instance().getAnnotatedFields(entityClass, annotationClass);
        }
        return fields;
    }

}
