package main.java.com.socratex.plugins.documentator.framework.helpers.files;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class TextFileManager {

    private static TextFileManager instance = new TextFileManager();

    private TextFileManager() {
    }

    public static TextFileManager instance() {
        return instance;
    }

    public List<String> readFromFile(String filePath) {
        try {
            Path path = Paths.get(filePath).toAbsolutePath();
            File file = assureFileExists(filePath);
            return Files.readAllLines(path);
        } catch (IOException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public void writeToFile(String filePath, String contents) {
        try {
            File file = assureFileExists(filePath);
            Files.write(Paths.get(filePath), contents.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public File assureFileExists(String filePath) throws IOException {
        File file = new File(filePath);
        File directory = file.getParentFile();
        if (!directory.exists()) {
            directory.mkdir();
        }
        if (!file.exists()) {
            file.createNewFile();
        }
        return file;
    }

}
