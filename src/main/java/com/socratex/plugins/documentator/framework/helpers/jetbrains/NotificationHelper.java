package main.java.com.socratex.plugins.documentator.framework.helpers.jetbrains;

import com.intellij.notification.Notification;
import com.intellij.notification.NotificationType;
import com.intellij.notification.Notifications;

public class NotificationHelper {

    public static void displayNotification(String message, NotificationType type) {
        Notifications.Bus.notify(new Notification("socratex.Documentator", "Documentator", message, type));
    }

}
