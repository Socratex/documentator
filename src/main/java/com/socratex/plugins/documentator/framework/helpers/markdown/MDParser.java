package main.java.com.socratex.plugins.documentator.framework.helpers.markdown;

import main.java.com.socratex.plugins.documentator.framework.helpers.markdown.table.MDTableColumn;

import java.util.Collection;

public class MDParser {

    private static MDParser instance = new MDParser();

    private MDParser() {
    }

    public static MDParser instance() {
        return instance;
    }

    public String createTable(Collection<MDTableColumn> columns) {
        StringBuilder builder = new StringBuilder();
        String columnDivider = MarkdownUtils.COLUMN_DIVIDER;
        String space = MarkdownUtils.SPACE;

        for (MDTableColumn column : columns) {
            builder.append(columnDivider + space + column.name() + space + columnDivider);
        }
        builder.append(columnDivider + MarkdownUtils.NEW_LINE);
        for (MDTableColumn column : columns) {
            builder.append(columnDivider + space + column.align().getCellContent() + space + columnDivider);
        }
        builder.append(columnDivider + MarkdownUtils.NEW_LINE);

        return builder.toString();
    }

    private String wrapToComment(String text) {
        return MarkdownUtils.COMMENT_START + text + MarkdownUtils.COMMENT_END;
    }

}
