package main.java.com.socratex.plugins.documentator.framework.helpers.markdown;

import main.java.com.socratex.plugins.documentator.framework.base.wrappers.CollectionExtractor;
import main.java.com.socratex.plugins.documentator.framework.helpers.StringTrimmer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

public class MarkdownUtils {

    public static final String SPACE = " ";

    public static final String COMMENT_START = "<!--";

    public static final String COMMENT_END = "-->";

    public static final String COLUMN_DIVIDER = "|";

    public static final String NEW_LINE = "\n";

    public static String comment(String content) {
        return COMMENT_START + content + COMMENT_END;
    }

    public static String closeComment(String content) {
        return COMMENT_START + "/" + content + COMMENT_END;
    }

    public static boolean isTableContentsDivider(String line) {
        return line.matches("^[| :-]*$");
    }

    public static Collection<String> extractTableRowValues(String line) {
        Collection<String> values = new ArrayList<>(Arrays.asList(line.split("\\|")));
        values = CollectionExtractor.createFor(StringTrimmer.instance()).extract(values);
        ((ArrayList) values).remove(0);
        return values;
    }

    public static String createTableRow(Collection<String> values) {
        return COLUMN_DIVIDER + String.join(COLUMN_DIVIDER, values) + COLUMN_DIVIDER;
    }

}
