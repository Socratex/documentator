package main.java.com.socratex.plugins.documentator.framework.helpers.markdown.table;

public enum MDColumnAlign {
    LEFT(":---"),
    CENTER(":---:"),
    RIGHT("---:");

    private String cellContent;

    MDColumnAlign(String cellContent) {

        this.cellContent = cellContent;
    }

    public String getCellContent() {
        return cellContent;
    }
}
