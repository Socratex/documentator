package main.java.com.socratex.plugins.documentator.framework.helpers.markdown.table;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface MDTableColumn {

    String name();

    MDColumnAlign align() default MDColumnAlign.LEFT;

}
