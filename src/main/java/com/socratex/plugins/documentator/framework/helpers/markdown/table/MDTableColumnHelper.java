package main.java.com.socratex.plugins.documentator.framework.helpers.markdown.table;

import main.java.com.socratex.plugins.documentator.framework.helpers.annotation.BaseAnnotatedFieldHelper;

public class MDTableColumnHelper extends BaseAnnotatedFieldHelper<MDTableColumn> {

    private static MDTableColumnHelper instance = new MDTableColumnHelper();

    private MDTableColumnHelper() {
    }

    public static MDTableColumnHelper instance() {
        return instance;
    }

    @Override
    protected Class<MDTableColumn> getAnnotationClass() {
        return MDTableColumn.class;
    }

}
