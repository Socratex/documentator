package main.java.com.socratex.plugins.documentator.framework.ui.base;

import javax.swing.*;

public interface ContentHandler<T extends JComponent> {

    void handleComponent(T component);

}
