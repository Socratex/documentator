package main.java.com.socratex.plugins.documentator.framework.ui.base;

import javax.swing.*;

public interface ContentManager<T extends JComponent> {

    void manageContent(T content);

}
