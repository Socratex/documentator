package main.java.com.socratex.plugins.documentator.framework.ui.base;

import javax.swing.*;

public interface ContentProvider<Component extends JComponent> {

    JComponent getContent();

}
