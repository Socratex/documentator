package main.java.com.socratex.plugins.documentator.framework.ui.base;

import com.intellij.openapi.util.Pair;

import javax.swing.*;

public class DefaultWindowSegment<T extends JComponent> implements ManagedSegment {

    private Pair<T, ContentManager<T>> managedSegment;

    public DefaultWindowSegment(T segment, ContentManager<T> segmentManager) {
        this.managedSegment = new Pair<>(segment, segmentManager);
    }

    @Override
    public void manageSegment() {
        managedSegment.second.manageContent(managedSegment.first);
    }

}
