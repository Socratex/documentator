package main.java.com.socratex.plugins.documentator.framework.ui.base;

public interface ManagedSegment {

    void manageSegment();

}
