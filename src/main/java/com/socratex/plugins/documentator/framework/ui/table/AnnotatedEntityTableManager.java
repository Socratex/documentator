package main.java.com.socratex.plugins.documentator.framework.ui.table;

import main.java.com.socratex.plugins.documentator.framework.base.Extractor;
import main.java.com.socratex.plugins.documentator.framework.base.Provider;
import main.java.com.socratex.plugins.documentator.framework.helpers.DataConverter;
import org.jetbrains.annotations.NotNull;

import javax.swing.table.DefaultTableModel;
import java.util.Collection;
import java.util.Set;

public class AnnotatedEntityTableManager<T> extends DefaultAbstractTableManager {

    private Class<T> entityClass;

    private Provider<Collection<? extends T>> entriesProvider;

    private Extractor<? super T, ? extends String[]> entryValueExtractor;

    public AnnotatedEntityTableManager(Class<T> entityClass, Provider<Collection<? extends T>> entriesProvider, Extractor<? super T, ? extends String[]> entryValueExtractor) {
        this.entityClass = entityClass;
        this.entriesProvider = entriesProvider;
        this.entryValueExtractor = entryValueExtractor;
    }

    @Override
    public void manageTableModel(DefaultTableModel tableModel) {
        Collection<? extends T> entries = entriesProvider.provide();
        for (T entry : entries) {
            tableModel.addRow(entryValueExtractor.extract(entry));
        }
    }

    @NotNull
    @Override
    public String[] createColumnIdentifiers() {
        Set<String> columnNames = JTableColumnHelper.instance().getColumnNames(entityClass);
        Object[] annotationsData = DataConverter.instance().setToArray(columnNames);
        return DataConverter.arrayCast(annotationsData, String[].class);
    }

}
