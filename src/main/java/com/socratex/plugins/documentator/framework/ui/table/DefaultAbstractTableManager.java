package main.java.com.socratex.plugins.documentator.framework.ui.table;

import main.java.com.socratex.plugins.documentator.framework.ui.base.ContentManager;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public abstract class DefaultAbstractTableManager implements ContentManager<JTable> {

    @Override
    public void manageContent(JTable content) {
        DefaultTableModel tableModel = (DefaultTableModel) content.getModel();
        tableModel.setColumnIdentifiers(createColumnIdentifiers());
        manageTableModel(tableModel);
        content.setModel(tableModel);
    }

    public abstract void manageTableModel(DefaultTableModel tableModel);

    @NotNull
    public abstract String[] createColumnIdentifiers();

}
