package main.java.com.socratex.plugins.documentator.framework.ui.table;

public enum JColumnAlign {
    LEFT, CENTER, RIGHT
}
