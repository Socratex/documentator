package main.java.com.socratex.plugins.documentator.framework.ui.table;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface JTableColumn {

    String name();

    boolean hidden() default false;

    JColumnAlign align() default JColumnAlign.LEFT;

}
