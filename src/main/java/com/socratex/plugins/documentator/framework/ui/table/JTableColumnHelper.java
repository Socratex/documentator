package main.java.com.socratex.plugins.documentator.framework.ui.table;

import main.java.com.socratex.plugins.documentator.framework.helpers.annotation.BaseAnnotatedFieldHelper;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

public class JTableColumnHelper extends BaseAnnotatedFieldHelper<JTableColumn> {

    private static JTableColumnHelper instance = new JTableColumnHelper();

    private Set<String> columnNames = null;

    private JTableColumnHelper() {
    }

    public static JTableColumnHelper instance() {
        return instance;
    }

    @Override
    protected Class<JTableColumn> getAnnotationClass() {
        return JTableColumn.class;
    }

    public Set<String> getColumnNames(Class<?> entityClass) {
        if (columnNames == null) {
            Collection<JTableColumn> annotations = getAnnotations(entityClass);
            columnNames = new LinkedHashSet<>();
            annotations.forEach(jTableColumn -> {
                String name = jTableColumn.name();
                columnNames.add(name);
            });
        }
        return columnNames;
    }

}
